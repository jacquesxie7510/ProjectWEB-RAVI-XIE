
			<!-- On vérifie si les champs ne sont pas vides-->	

	<?php 

	if(!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])){

		//connexion avec la base de donnée 

		require_once '../../view/inc/bdd.php';
		
		
		$req = $pdo->prepare('SELECT * FROM users WHERE username = :username OR email = :username');
		$req-> execute(['username'=> $_POST['username']]);
		

		
		
		//on vérifie si le mot de passe correspond au nom d'utilisateur 

		if (($user = $req->fetch(PDO::FETCH_OBJ)) && password_verify($_POST['password'], $user->password)){

			session_start();
			$_SESSION['auth'] = $user;

			header('Location: account.php');

			exit();
			
			
		}else{


			$errors['password']= 'pseudo ou Mot de passe incorrect!';
		}

		

		
	}

	?>

	<!-- affiche les erreurs -->

	<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p> vous n'avez pas rempli correctement le formulaire </p>
		<ul>
		<?php foreach($errors as $error): ?>
		<li><?= $error; ?></li>
		<?php endforeach; ?>
	   </ul>

		</div>

		<?php endif; ?> 
<?php require '../../view/inc/header.php';?>
	
	<header class="intro-header" style="background-image: url('../../view/img/cocktail2.jpg')"> 

	<?php require '../../view/inc/middle.php'; ?>
	
	<h1> Connexion </h1>

	<!-- Formulaire-->

<form action= "" method="POST">

		<div class="form-group">

			<label for=""> Pseudo ou email</label>

			<input type="text" name="username" class="form-control" required/>

		</div>


		<div class="form-group">

			<label for=""> Mot de passe </label>

			<input type="password" name="password" class="form-control" required/>
			
		</div>


		

		<button type="submit" class="btn btn-primary"> Se connecter</button>

	</form>

	<?php require '../../view/inc/footer.php'; ?>