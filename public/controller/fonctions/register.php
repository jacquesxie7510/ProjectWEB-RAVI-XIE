	<?php require '../../view/inc/header.php'; ?>

	<header class="intro-header" style="background-image: url('../../view/img/cocktail3.jpeg')"> 

	<?php require '../../view/inc/middle.php'; ?>

	<?php

	if(!empty($_POST)){

		$errors = array();
		require_once '../../view/inc/bdd.php';

		if(empty($_POST['username'])){
			$errors['username']='Veuillez entrer un pseudo';
		}else{

			//on cherche si l'utilisateur existe déjà

			$req =$pdo->prepare('SELECT id FROM users WHERE username=?');
			$req->execute([$_POST['username']]);
			$user= $req->fetch();
			if($user){
				$errors['username']= 'Ce pseudo est déjà pris';
			}

		}

		if(empty($_POST['email'])){
			$errors['email']='Veuillez entrer un pseudo';
		}else{

			//on cherche si l'utilisateur existe

			$req =$pdo->prepare('SELECT id FROM users WHERE email=?');
			$req->execute([$_POST['email']]);
			$user= $req->fetch();
			if($user){
				$errors['email']= 'cet email est déjà pris';
			}

		}

		// on cherche a savoir si les deux mots de passe sont les mêmes

		if (empty($_POST['password']) || ($_POST['password'] != $_POST['password_confirm'])) {

			$errors['password']= ' Le mot de passe et la confirmation de mot de passe doient être identique';
			
		}

		if(empty($errors)){

				//on insert les informations dans la base de donnée
			
		$req= $pdo->prepare("INSERT INTO users SET username= ?,password= ?, email= ?");

		//on crypte le mot de passe

		$password = password_hash($_POST['password'],PASSWORD_BCRYPT);
		 $req->execute([$_POST['username'],$password,$_POST['email']]); 
		 
		
			
			die('Vous êtes inscrit ! vous pouvez vous connecter!');
		
		}
		
		
	}

				?>

	
	<h1>  inscription </h1>

	<!-- On affiche les erreurs -->

<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p> vous n'avez pas rempli correctement le formulaire: </p>

	</br>
		<ul>
		<?php foreach($errors as $error): ?>
		<li><?= $error; ?></li>
		<?php endforeach; ?>
	   </ul>

		</div>

		<?php endif; ?> 

		<!-- formulaire-->

	<form action= "" method="POST">

		<div class="form-group">

			<label for=""> Pseudo</label>

			<input type="text" name="username" class="form-control" required/>

		</div>

		<div class="form-group">

			<label for=""> Email</label>

			<input type="email" name="email" class="form-control" required/>
			
		</div>


		<div class="form-group">

			<label for=""> Mot de passe</label>

			<input type="password" name="password" class="form-control" required/>
			
		</div>


		<div class="form-group">

			<label for=""> Confirmez le mot de passe</label>

			<input type="password" name="password_confirm" class="form-control" required/>
			
		</div>

		<button type="submit" class="btn btn-primary"> M'inscrire</button>

	</form>

<?php require '../../view/inc/footer.php'; ?>