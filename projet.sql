-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 25 Novembre 2016 à 19:54
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `boire`
--

CREATE TABLE `boire` (
  `ID` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `recette` text NOT NULL,
  `photo` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `boire`
--

INSERT INTO `boire` (`ID`, `titre`, `recette`, `photo`) VALUES
(1, 'Amaretto Sour ', '- Amaretto\r\n- Jus de citron\r\n- Sucre de canne', '../../view/img/ama_sour.jpg'),
(2, 'Caipirinha', '- Cachaca\r\n- Citron vert \r\n- Sucre', '../../view/img/caipirinha-cocktail.jpg'),
(3, 'CCC', '	jus de pomme- vodka\r\n				', '../../view/img/ccc.png'),
(4, 'Spritz', '- 4 cl Eau Gazeuse\r\n- 8 cl Martini Bitter\r\n- 12 cl Martini Prosecco', '../../view/img/spritz.jpg'),
(5, 'apicius', '5 cl Martini Rose - 1 cl Liqueur de fraise des bois - 3 cl Vodka', '../../view/img/Apicius.jpg'),
(6, 'Tequilla', '- Citron\r\n- Tequilla', '../../view/img/tequila.jpg'),
(7, 'mojito fraise', 'fraise et rhum', '../../view/img/mojito-fraise.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `boire`
--
ALTER TABLE `boire`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `boire`
--
ALTER TABLE `boire`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
